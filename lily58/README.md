# How to configure a keyboard

- Go to https://config.qmk.fm/#/lily58/rev1/LAYOUT
- Choose the layout you want and configure to your preferences.
- Download the keymap.json
- Move to the correct location, e.g. `$HOME/Documents/keyboards/lily58`
- Go into the qmk_firmware directory, to the subdirectory of your keyboard, e.g. `keyboards/lily58/keymaps/mine`
- Generate the .c file: `qmk json2c -o keymap.c $HOME/Documents/keyboards/lily58/lily58_rev1_layout_mine.json`
- In the same directory, run `qmk compile && qmk flash`

# Using the make command
- Initialize the qmk_toolbox setup
- Symlink the `natjo` directory to qmk_firmware/keyboards/lily58/keymaps/natjo
- In the main dirctory of qmk_firmware run `make lily58:natjo:avrdude`


