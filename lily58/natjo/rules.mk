BOOTLOADER = caterina

EXTRAKEY_ENABLE = yes
# WPM_ENABLE = yes

# Defaults from the autogeneration
BOOTMAGIC_ENABLE = no       # Enable Bootmagic Lite
MOUSEKEY_ENABLE = no        # Mouse keys
COMMAND_ENABLE = no         # Commands for debug and configuration
NKRO_ENABLE = no
BACKLIGHT_ENABLE = no       # Enable keyboard backlight functionality
AUDIO_ENABLE = no           # Audio output
RGBLIGHT_ENABLE = no        # Enable WS2812 RGB underlight.
SWAP_HANDS_ENABLE = no      # Enable one-hand typing
OLED_ENABLE= yes     		# OLED display

# If you want to change the display of OLED, you need to change here
SRC +=  ./lib/logo_reader.c \
